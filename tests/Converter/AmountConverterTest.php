<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Converter;

use Jtl\Vouchers\Api\Sdk\Converter\AmountConverter;
use PHPUnit\Framework\TestCase;

/**
 * Class AmountConverterTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Converter
 */
class AmountConverterTest extends TestCase
{
    public function testCanConvertStringAmountToFloat(): void
    {
        self::assertEquals(12.67, AmountConverter::toFloat('12.67'));
        self::assertEquals(121.67, AmountConverter::toFloat('121.67'));
        self::assertEquals(1213.67, AmountConverter::toFloat('1213.67'));
        self::assertEquals(12132.67, AmountConverter::toFloat('12132.67'));
        self::assertEquals(12132.67, AmountConverter::toFloat('12,132.67'));
    }

    public function testCanConvertFloatAmountToString(): void
    {
        self::assertEquals('12.67', AmountConverter::toString(12.67));
        self::assertEquals('121.67', AmountConverter::toString(121.67));
        self::assertEquals('1213.67', AmountConverter::toString(1213.67));
        self::assertEquals('12132.67', AmountConverter::toString(12132.67));
    }
}
