<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Converter;

use Jtl\Vouchers\Api\Sdk\Converter\TaxRateConverter;
use PHPUnit\Framework\TestCase;

/**
 * Class TaxRateConverterTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Converter
 */
class TaxRateConverterTest extends TestCase
{
    public function testCanConvertStringAmountToFloat(): void
    {
        self::assertEquals(12.67, TaxRateConverter::toFloat('12.67'));
        self::assertEquals(121.67, TaxRateConverter::toFloat('121.67'));
        self::assertEquals(1213.67, TaxRateConverter::toFloat('1213.67'));
        self::assertEquals(12132.67, TaxRateConverter::toFloat('12132.67'));
        self::assertEquals(12132.67, TaxRateConverter::toFloat('12,132.67'));
    }
    
    public function testCanConvertFloatAmountToString(): void
    {
        self::assertEquals('12.67', TaxRateConverter::toString(12.67));
        self::assertEquals('121.67', TaxRateConverter::toString(121.67));
        self::assertEquals('1213.67', TaxRateConverter::toString(1213.67));
        self::assertEquals('12132.67', TaxRateConverter::toString(12132.67));
    }
}
