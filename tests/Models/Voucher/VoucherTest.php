<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Models\Voucher;

use Jtl\Vouchers\Api\Sdk\Models\Voucher\Voucher;
use PHPUnit\Framework\TestCase;

class VoucherTest extends TestCase
{
    public function testCanExtractData(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/voucher_one.json');
        $voucher = new Voucher(json_decode($json, true, 512, JSON_THROW_ON_ERROR));

        self::assertEquals('6dae40fa-cae0-11e7-80b6-8c85901eed2e', $voucher->getId());
        self::assertEquals('6cde40fa-cae0-11e8-80b6-8c85901eed2e', $voucher->getClientId());
        self::assertEquals('50.56', $voucher->getAmount());
        self::assertEquals(50.56, $voucher->getAmountAsFloat());
        self::assertEquals(false, $voucher->isTaxable());
    }
    
    public function testVoucherCanBeTaxable(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/voucher_one_taxable.json');
        $voucher = new Voucher(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
        
        self::assertEquals('6dae40fa-cae0-11e7-80b6-8c85901eed2e', $voucher->getId());
        self::assertEquals('6cde40fa-cae0-11e8-80b6-8c85901eed2e', $voucher->getClientId());
        self::assertEquals('19.5', $voucher->getTaxRate());
        self::assertEquals(19.5, $voucher->getTaxRateAsFloat());
        self::assertEquals(true, $voucher->isTaxable());
    }

    public function testCanConvertedToArray(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/voucher_one.json');

        $voucher = new Voucher(json_decode($json, true, 512, JSON_THROW_ON_ERROR));

        self::assertIsArray($voucher->toArray());
    }
}
