<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Models\Reservation;

use Jtl\Vouchers\Api\Sdk\Models\Reservation\Reservation;
use PHPUnit\Framework\TestCase;

class ReservationTest extends TestCase
{
    public function testCanExtractData(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/reservation_one.json');
        $reservation = new Reservation(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
        
        self::assertEquals('6dae50fa-cae0-11e7-80b6-8d86701fdd2e', $reservation->getId());
        self::assertEquals('6dae40fa-cae0-11e7-80b6-8c85901eed2e', $reservation->getVoucherId());
        self::assertEquals('6cde40fa-cae0-11e8-80b6-8c85901eed2e', $reservation->getClientId());
        self::assertEquals('10.53', $reservation->getAmount());
        self::assertEquals(10.53, $reservation->getAmountAsFloat());
        self::assertEquals(false, $reservation->getVoucherTaxable());
        self::assertEquals(null, $reservation->getVoucherTaxRate());
        self::assertEquals('ORDER-62642', $reservation->getOrderNumber());
    }
    
    public function testCanBeTaxable(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/reservation_one_taxable.json');
        $reservation = new Reservation(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
    
        self::assertEquals('19.5', $reservation->getVoucherTaxRate());
        self::assertEquals(true, $reservation->getVoucherTaxable());
        self::assertEquals(19.5, $reservation->getVoucherTaxRateAsFloat());
    }
    
    public function testCanConvertedToArray(): void
    {
        $json = file_get_contents(__DIR__ . '/../../Resources/MockData/reservation_one.json');
        
        $reservation = new Reservation(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
    
        self::assertIsArray($reservation->toArray());
    }
}
