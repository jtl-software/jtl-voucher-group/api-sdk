<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface;
use Jtl\Vouchers\Api\Sdk\Client;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientTest
 * @package Jtl\Vouchers\Api\Sdk\Test
 */
class ClientTest extends TestCase
{
    public function testCanBeCreated(): void
    {
        $client = new Client();
        
        $this->assertInstanceOf(
            Client::class,
            $client
        );
    }
    
    /**
     * @depends testCanBeCreated
     */
    public function testCanBeCreatedWithConfig(): void
    {
        $client = $this->createClientWithConfig();
        
        self::assertIsArray($client->getConfig());
        self::assertCount(2, $client->getConfig());
        
        $client->setConfig(['foo' => 'bar']);
        self::assertCount(1, $client->getConfig());
        self::assertArrayHasKey('foo', $client->getConfig());
    }
    
    /**
     * @depends testCanBeCreated
     */
    public function testCanHoldHttpClientInterface(): void
    {
        $client = $this->createClientWithConfig();
        
        $http = new HttpClient();
        $client->setHttp($http);
    
        self::assertInstanceOf(HttpClient::class, $client->getHttp());
    }
    
    /**
     * @depends testCanBeCreated
     */
    public function testCanCreateDefaultHttpClient(): void
    {
        $client = $this->createClientWithConfig();
    
        self::assertInstanceOf(ClientInterface::class, $client->getHttp());
    }
    
    /**
     * @return Client
     */
    private function createClientWithConfig(): Client
    {
        return new Client([
            'base_uri' => 'https://www.jtl-software.de',
            'token' => 'foobar'
        ]);
    }
}
