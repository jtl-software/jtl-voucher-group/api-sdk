<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Resources;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Models\Reservation\Reservation;
use Jtl\Vouchers\Api\Sdk\Models\Charge\Charge;
use Jtl\Vouchers\Api\Sdk\Resources\ReservationResource;

/**
 * Class ReservationTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Resources
 */
class ReservationTest extends AbstractResourceTest
{
    public function testCanReserve(): void
    {
        $mock = new MockHandler([
            new Response(201, [], file_get_contents(__DIR__ . '/MockData/reservation_one.json')),
            new Response(422),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
        
        $resource = new ReservationResource($client);
    
        $reservation = $resource->reserve('10.53', 'EUR', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    
        self::assertInstanceOf(Reservation::class, $reservation);
        self::assertNotNull($reservation);
        self::assertEquals('10.53', $reservation->getAmount());
    
        $this->expectException(ClientException::class);
        $resource->reserve('10.53', 'EUR', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    
        $this->expectException(ClientException::class);
        $resource->reserve('10.53', 'EUR', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    
        $this->expectException(ClientException::class);
        $resource->reserve('10.53', 'EUR', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    }
    
    public function testCanCharge(): void
    {
        $mock = new MockHandler([
            new Response(201, [], file_get_contents(__DIR__ . '/MockData/charge_one.json')),
            new Response(422),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
    
        $resource = new ReservationResource($client);
    
        $charge = $resource->charge('6dae50fa-cae0-11e7-80b6-8d86701fdd2e', 'ORDER-62642');

        self::assertInstanceOf(Charge::class, $charge);
        self::assertNotNull($charge);
        self::assertEquals('10.53', $charge->getAmount());
        self::assertEquals('ORDER-62642', $charge->getOrderNumber());
    
        $this->expectException(ClientException::class);
        $resource->charge('10.53', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    
        $this->expectException(ClientException::class);
        $resource->charge('10.53', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    
        $this->expectException(ClientException::class);
        $resource->charge('10.53', '6K99-LACJ-QR4E-74J4', 'MSCUS4X3');
    }
}
