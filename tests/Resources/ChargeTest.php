<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Resources;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Resources\ChargeResource;

/**
 * Class ChargeTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Resources
 */
class ChargeTest extends AbstractResourceTest
{
    public function testCanCancelCharge(): void
    {
        $mock = new MockHandler([
            new Response(204, [], '"No Content"'),
            new Response(422),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
    
        $resource = new ChargeResource($client);
    
        $result = $resource->cancel('6dae50fa-cae0-11e7-80b6-8d86701fdd2f');
        
        self::assertTrue($result);
    
        $this->expectException(ClientException::class);
        $resource->cancel('6dae50fa-cae0-11e7-80b6-8d86701fdd2f');
    
        $this->expectException(ClientException::class);
        $resource->cancel('6dae50fa-cae0-11e7-80b6-8d86701fdd2f');
    
        $this->expectException(ClientException::class);
        $resource->cancel('6dae50fa-cae0-11e7-80b6-8d86701fdd2f');
    }
}
