<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Resources;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use Izzle\Model\Model;
use Izzle\Model\PropertyInfo;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Pagination;
use Jtl\Vouchers\Api\Sdk\Models\Query;
use Jtl\Vouchers\Api\Sdk\Resources\ResourceInterface;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client as HttpClient;

/**
 * Class AbstractResourceTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Resources
 */
abstract class AbstractResourceTest extends TestCase
{
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $method
     * @param callable|null $assertions
     */
    public function canQueryAll(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $method = 'all',
        callable $assertions = null
    ): void {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents($jsonFile)),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Pagination $result */
        $result = $resource->{$method}(new Query());
        $items = $result->getData();
        
        self::assertCount(2, $items);
        self::assertInstanceOf($resultClass, $items[0]);
        
        $this->expectException(ClientException::class);
        $resource->{$method}(new Query());
    
        self::assertEmpty($resource->{$method}(new Query()));
        
        if ($assertions !== null) {
            $assertions($result);
        }
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $id
     * @param PropertyInfo $idProperty
     */
    public function canFindOne(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $id,
        PropertyInfo $idProperty
    ): void {
        $client = $this->prepareHttpHandler(file_get_contents($jsonFile));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        $model = $resource->find($id);
        $getter = $idProperty->getter();
    
        self::assertInstanceOf($resultClass, $model);
        self::assertEquals($id, $model->{$getter}());
        
        $this->expectException(ClientException::class);
        $resource->find($id);
    
        self::assertEmpty($resource->find($id));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $method
     * @param string $id
     * @param PropertyInfo|null $idProperty
     * @throws InvalidArgumentException
     */
    public function canFindOneBy(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $method,
        string $id,
        PropertyInfo $idProperty = null
    ): void {
        if (!is_callable([$resourceClass, $method])) {
            throw new InvalidArgumentException(sprintf('Method %s is not callable on %s', $method, $resourceClass));
        }
        
        $client = $this->prepareHttpHandler(file_get_contents($jsonFile));
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        $model = $resource->{$method}($id);
        
        if ($idProperty !== null) {
            $getter = $idProperty->getter();
            self::assertEquals($id, $model->{$getter}());
        }
    
        self::assertInstanceOf($resultClass, $model);
        
        $this->expectException(ClientException::class);
        $resource->{$method}($id);
    
        self::assertEmpty($resource->{$method}($id));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string $id
     * @param PropertyInfo $idProperty
     * @param Query|null $query
     */
    public function canUpdate(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        string $id,
        PropertyInfo $idProperty,
        Query $query = null
    ): void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler(null, 200);
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
        $result = $resource->save($model, $query);
        
        $getter = $idProperty->getter();
        
        self::assertTrue($result);
        self::assertEquals($id, $model->{$getter}());
        
        $this->expectException(ClientException::class);
        $resource->save($model, $query);
    
        self::assertEmpty($resource->save($model, $query));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param string|null $id
     * @param PropertyInfo|null $idProperty
     * @param Query|null $query
     */
    public function canCreate(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        ?string $id = null,
        PropertyInfo $idProperty = null,
        Query $query = null
    ):void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler($json, 201);
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
        
        if ($id !== null && $idProperty !== null) {
            $getter = $idProperty->getter();
            $setter = $idProperty->setter();
            
            $model->{$setter}(null);
        }
        
        $result = $resource->save($model, $query);
        
        self::assertTrue($result);
        
        if ($id !== null && $idProperty !== null) {
            self::assertEquals($id, $model->{$getter}());
        }
        
        $this->expectException(ClientException::class);
        $resource->save($model, $query);
    
        self::assertEmpty($resource->save($model, $query));
    }
    
    /**
     * @param string $jsonFile
     * @param string $resourceClass
     * @param string $resultClass
     * @param Query|null $query
     */
    public function canRemove(
        string $jsonFile,
        string $resourceClass,
        string $resultClass,
        Query $query = null
    ): void {
        $json = file_get_contents($jsonFile);
        $client = $this->prepareHttpHandler(null, 204);
        
        /** @var ResourceInterface $resource */
        $resource = new $resourceClass($client);
        
        /** @var Model $model */
        $model = new $resultClass(json_decode($json, true, 512, JSON_THROW_ON_ERROR));
    
        self::assertTrue($resource->remove($model, $query));
        
        $this->expectException(ClientException::class);
        $resource->remove($model, $query);
    
        self::assertFalse($resource->remove($model, $query));
    }
    
    /**
     * @param string|null $json
     * @param int $success
     * @param int $forbidden
     * @param int $notFound
     * @return Client
     */
    protected function prepareHttpHandler(
        string $json = null,
        int $success = 200,
        int $forbidden = 403,
        int $notFound = 404
    ): Client {
        $response = $json !== null ?
            new Response($success, [], $json) :
            new Response($success);
        
        $mock = new MockHandler([
            $response,
            new Response($forbidden),
            new Response($notFound)
        ]);
        
        $handler = HandlerStack::create($mock);
        
        return (new Client())->setHttp(new HttpClient(['handler' => $handler]));
    }
}
