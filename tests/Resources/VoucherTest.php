<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Resources;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Izzle\Model\PropertyInfo;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Pagination;
use Jtl\Vouchers\Api\Sdk\Models\Query;
use Jtl\Vouchers\Api\Sdk\Models\Voucher\Status;
use Jtl\Vouchers\Api\Sdk\Models\Charge\Charge;
use Jtl\Vouchers\Api\Sdk\Models\Voucher\Voucher;
use Jtl\Vouchers\Api\Sdk\Resources\VoucherResource;

/**
 * Class VoucherTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Resources
 */
class VoucherTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/voucher_all.json', VoucherResource::class, Voucher::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/voucher_one.json',
            VoucherResource::class,
            Voucher::class,
            '6dae40fa-cae0-11e7-80b6-8c85901eed2e',
            new PropertyInfo('id')
        );
    }

    public function testCanFindOneComplete(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/voucher_one.json'));

        $resource = new VoucherResource($client);

        $id = '6dae40fa-cae0-11e7-80b6-8c85901eed2e';

        $model = $resource->findComplete($id);

        self::assertInstanceOf(Voucher::class, $model);
        self::assertEquals($id, $model->getId());

        $this->expectException(ClientException::class);
        $resource->find($id);

        self::assertEmpty($resource->find($id));
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/voucher_one.json',
            VoucherResource::class,
            Voucher::class,
            '6dae40fa-cae0-11e7-80b6-8c85901eed2e',
            new PropertyInfo('id')
        );
    }
    
    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/voucher_one.json',
            VoucherResource::class,
            Voucher::class
        );
    }

    public function testCanRecharge(): void
    {
        $mock = new MockHandler([
            new Response(201, [], file_get_contents(__DIR__ . '/MockData/voucher_recharge.json')),
            new Response(404)
        ]);

        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));

        $resource = new VoucherResource($client);

        self::assertTrue($resource->recharge('50.00', 'EUR', 'foobar123', 'cff8784e34b28b9b04860'));

        $this->expectException(ClientException::class);
        self::assertFalse($resource->recharge('50.00', 'EUR', 'foobar123', 'cff8784e34b28b9b04860'));
    }
    
    public function testCanSetAStatus(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/voucher_one.json')),
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/voucher_one.json')),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
        
        $resource = new VoucherResource($client);
        
        self::assertTrue($resource->setStatus('JKWCCVOHEE24MJG3', Status::ACTIVE));
        self::assertTrue($resource->setStatus('JKWCCVOHEE24MJG3', Status::ACTIVE, 'test'));
    
        $this->expectException(ClientException::class);
        self::assertFalse($resource->setStatus('JKWCCVOHEE24MJG3', Status::ACTIVE));
    
        $this->expectException(ClientException::class);
        self::assertFalse($resource->setStatus('JKWCCVOHEE24MJG3', Status::ACTIVE));
    }
    
    public function testCanGetAllCharges(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/charge_all.json')),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
    
        $resource = new VoucherResource($client);
    
        /** @var Pagination $result */
        $result = $resource->allCharges('6dae40fa-cae0-11e7-80b6-8c85901eed2e', new Query());
        $items = $result->getData();
    
        self::assertCount(2, $items);
        self::assertInstanceOf(Charge::class, $items[0]);
    
        $this->expectException(ClientException::class);
        $resource->allCharges('6dae40fa-cae0-11e7-80b6-8c85901eed2e', new Query());
    
        self::assertEmpty($resource->allCharges('6dae40fa-cae0-11e7-80b6-8c85901eed2e', new Query()));
    }
    
    public function testCanGetACharge(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/charge_one.json')),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
    
        $resource = new VoucherResource($client);
    
        $charge = $resource->findCharge(
            '6dae40fa-cae0-11e7-80b6-8c85901eed2e',
            '6dae50fa-cae0-11e7-80b6-8d86701fdd2f',
            new Query()
        );

        self::assertInstanceOf(Charge::class, $charge);
        self::assertEquals('10.53', $charge->getAmount());
        self::assertEquals(Charge::TYPE_CHARGE, $charge->getType());
    }
}
