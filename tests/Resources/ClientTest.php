<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test\Resources;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Resources\ClientResource;

/**
 * Class ClientTest
 * @package Jtl\Vouchers\Api\Sdk\Test\Resources
 */
class ClientTest extends AbstractResourceTest
{
    public function testCanConnect(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/client_one.json')),
            new Response(401),
            new Response(404),
            new Response(422),
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));
        
        $resource = new ClientResource($client);
        
        $voucherClient = $resource->connect('X53HI0');
        
        self::assertNotNull($voucherClient);
        self::assertEquals('da39a3ee5eb28b7392ef5', $voucherClient->getId());
        self::assertEquals('cff8784e5eb28b81ca02a', $voucherClient->getClientGroupId());
        self::assertEquals('Test-Client', $voucherClient->getName());
        self::assertCount(1, $voucherClient->getScopes());
        self::assertNotNull($voucherClient->getConnectedAt());
        
        $this->expectException(ClientException::class);
        $resource->connect('X53HI0');
        
        $this->expectException(ClientException::class);
        $resource->connect('X53HI0');
        
        $this->expectException(ClientException::class);
        $resource->connect('X53HI0');
    }

    public function testCanDisconnect(): void
    {
        $mock = new MockHandler([
            new Response(204),
            new Response(404)
        ]);

        $handler = HandlerStack::create($mock);
        $client = (new Client())->setHttp(new HttpClient(['handler' => $handler]));

        $resource = new ClientResource($client);

        self::assertTrue($resource->disconnect());

        $this->expectException(ClientException::class);
        $resource->disconnect();
    }
}
