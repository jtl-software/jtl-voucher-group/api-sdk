<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Test;

use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Resources\ClientResource;
use Jtl\Vouchers\Api\Sdk\Resources\VoucherResource;
use Jtl\Vouchers\Api\Sdk\Service;
use PHPUnit\Framework\TestCase;

/**
 * Class ServiceTest
 * @package Jtl\Vouchers\Api\Sdk\Test
 */
class ServiceTest extends TestCase
{
    public function testCanCreateAnInstance(): void
    {
        $client = new Client();
        $service = Service::boot($client);
        
        self::assertEquals($service->getClient(), $client);
        self::assertEquals(Service::getInstance(), $service);
    }
    
    /**
     * @depends testCanCreateAnInstance
     */
    public function testCanSetHaveAClient(): void
    {
        $client1 = new Client();
        $client2 = new Client();
    
        $service = Service::boot($client1);
        
        self::assertSame($service->getClient(), $client1);
        self::assertNotSame($service->getClient(), $client2);
    
        $service->setClient($client2);
        self::assertSame($service->getClient(), $client2);
        self::assertNotSame($service->getClient(), $client1);
    }
    
    /**
     * @depends testCanCreateAnInstance
     */
    public function testHaveResourceGetter(): void
    {
        $client = new Client();
        $service = Service::boot($client);

        // Voucher Resource
        self::assertNotNull($service->voucher());
        self::assertInstanceOf(VoucherResource::class, $service->voucher());
        
        // Client Resource
        self::assertNotNull($service->client());
        self::assertInstanceOf(ClientResource::class, $service->client());
    }
    
    public function testHaveVoucherResourceGetter(): void
    {
        $client = new Client();
        $service = Service::boot($client);
        
        self::assertTrue(
            method_exists($service, 'voucher'),
            sprintf('Class (%s) does not have method voucher', Service::class)
        );

        self::assertNotNull($service->voucher());
    }
    
    public function testHaveReservationResourceGetter(): void
    {
        $client = new Client();
        $service = Service::boot($client);
        
        self::assertTrue(
            method_exists($service, 'reservation'),
            sprintf('Class (%s) does not have method reservation', Service::class)
        );
        
        self::assertNotNull($service->reservation());
    }
    
    public function testHaveChargeResourceGetter(): void
    {
        $client = new Client();
        $service = Service::boot($client);
        
        self::assertTrue(
            method_exists($service, 'charge'),
            sprintf('Class (%s) does not have method charge', Service::class)
        );
        
        self::assertNotNull($service->charge());
    }
}
