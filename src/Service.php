<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk;

use Jtl\Vouchers\Api\Sdk\Exceptions\MissingApiClientException;
use Jtl\Vouchers\Api\Sdk\Resources\ClientResource;
use Jtl\Vouchers\Api\Sdk\Resources\ReservationResource;
use Jtl\Vouchers\Api\Sdk\Resources\ResourceInterface;
use Jtl\Vouchers\Api\Sdk\Resources\ChargeResource;
use Jtl\Vouchers\Api\Sdk\Resources\VoucherResource;
use RuntimeException;

/**
 * Class Service
 * @package Jtl\Vouchers\Api\Sdk
 */
class Service
{
    /**
     * @var Service|null
     */
    protected static ?Service $instance = null;
    
    /**
     * @var ClientInterface|null
     */
    protected ?ClientInterface $client = null;
    
    /**
     * Service constructor.
     * @param ClientInterface|null $client
     */
    protected function __construct(?ClientInterface $client = null)
    {
        $this->client = $client;
    }
    
    /**
     * @return self
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            throw new RuntimeException('Please run Service::boot first');
        }
        
        return self::$instance;
    }
    
    /**
     * @param ClientInterface $client
     * @return self
     */
    public static function boot(ClientInterface $client): self
    {
        self::$instance = new self($client);
        
        return self::$instance;
    }
    
    /**
     * @param ClientInterface $client
     * @return $this
     */
    public function setClient(ClientInterface $client): self
    {
        $this->client = $client;
        
        return $this;
    }
    
    /**
     * @return ClientInterface|null
     */
    public function getClient(): ?ClientInterface
    {
        return $this->client;
    }
    
    /**
     * @return VoucherResource|ResourceInterface
     * @throws MissingApiClientException
     */
    public function voucher(): VoucherResource
    {
        return $this->factory(VoucherResource::class);
    }
    
    /**
     * @return ReservationResource|ResourceInterface
     * @throws MissingApiClientException
     */
    public function reservation(): ReservationResource
    {
        return $this->factory(ReservationResource::class);
    }
    
    /**
     * @return ChargeResource|ResourceInterface
     * @throws MissingApiClientException
     */
    public function charge(): ChargeResource
    {
        return $this->factory(ChargeResource::class);
    }
    
    /**
     * @return ClientResource|ResourceInterface
     * @throws MissingApiClientException
     */
    public function client(): ClientResource
    {
        return $this->factory(ClientResource::class);
    }
    
    /**
     * @param string $class
     * @param ClientInterface|null $client
     * @return ResourceInterface
     * @throws MissingApiClientException
     */
    protected function factory(string $class, ?ClientInterface $client = null): ResourceInterface
    {
        $client = $client ?? $this->client;
        $this->checkClient($client);
        
        return new $class($client);
    }
    
    /**
     * @param ClientInterface|null $client
     * @throws MissingApiClientException
     */
    protected function checkClient(?ClientInterface $client = null): void
    {
        if ($client === null) {
            throw new MissingApiClientException(__METHOD__ . ' - ' . 'Api Client is null.');
        }
    }
}
