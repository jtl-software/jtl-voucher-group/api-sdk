<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk;

use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Client as HttpClient;

/**
 * Class Client
 * @package Jtl\Vouchers\Api\Sdk
 */
class Client implements ClientInterface
{
    /**
     * @var GuzzleClientInterface|null
     */
    private ?GuzzleClientInterface $http = null;
    
    /**
     * @var array
     */
    private array $config;
    
    /**
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = array_merge([
            'base_uri' => '',
            'token' => ''
        ], $config);
    }
    
    /**
     * @return GuzzleClientInterface
     */
    public function getHttp(): GuzzleClientInterface
    {
        if ($this->http === null) {
            $this->http = $this->createDefaultHttp();
        }
        
        return $this->http;
    }
    
    /**
     * @param GuzzleClientInterface $http
     * @return self
     */
    public function setHttp(GuzzleClientInterface $http): self
    {
        $this->http = $http;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
    
    /**
     * @param array $config
     * @return self
     */
    public function setConfig(array $config): self
    {
        $this->config = $config;
        
        return $this;
    }
    
    /**
     * @return HttpClient
     */
    protected function createDefaultHttp(): HttpClient
    {
        $options = [
            'base_uri' => $this->config['base_uri'],
            'headers' => [
                'Authorization' => 'Bearer ' . $this->config['token'],
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
        ];
        
        if (isset($this->config['guzzle_options'])  && is_array($this->config['guzzle_options'])) {
            $options = array_merge($options, $this->config['guzzle_options']);
        }
        
        return new HttpClient($options);
    }
}
