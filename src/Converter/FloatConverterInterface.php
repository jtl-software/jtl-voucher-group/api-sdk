<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Converter;

/**
 * Interface FloatConverterInterface
 * @package Jtl\Vouchers\Api\Sdk\Converter
 */
interface FloatConverterInterface
{
    /**
     * @param string $number
     * @return float
     */
    public static function toFloat(string $number): float;
    
    /**
     * @param float $number
     * @return string
     */
    public static function toString(float $number): string;
}
