<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Converter;

/**
 * Class AmountConverter
 * @package Jtl\Vouchers\Api\Sdk\Converter
 */
class AmountConverter implements FloatConverterInterface
{
    /**
     * @param string $amount - ^\d+\.\d{2}$
     * @return float
     */
    public static function toFloat(string $amount): float
    {
        return (float) str_replace(',', '', $amount);
    }

    /**
     * @param float $amount
     * @return string - ^\d+\.\d{2}$
     */
    public static function toString(float $amount): string
    {
        return number_format($amount, 2, '.', '');
    }
}
