<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Converter;

/**
 * Class TaxRateConverter
 * @package Jtl\Vouchers\Api\Sdk\Converter
 */
class TaxRateConverter implements FloatConverterInterface
{
    /**
     * @param string $taxRate - ^\d+\.\d{2}$
     * @return float
     */
    public static function toFloat(string $taxRate): float
    {
        return (float) str_replace(',', '', $taxRate);
    }
    
    /**
     * @param float $taxRate
     * @return string - ^\d+\.\d{2}$
     */
    public static function toString(float $taxRate): string
    {
        return number_format($taxRate, 2, '.', '');
    }
}
