<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk;

use GuzzleHttp\ClientInterface as GuzzleClientInterface;

/**
 * Interface ClientInterface
 * @package Jtl\Vouchers\Api\Sdk
 */
interface ClientInterface
{
    /**
     * @return GuzzleClientInterface
     */
    public function getHttp(): GuzzleClientInterface;
    
    /**
     * @param GuzzleClientInterface $http
     * @return $this
     */
    public function setHttp(GuzzleClientInterface $http): self;
    
    /**
     * @return array
     */
    public function getConfig(): array;
    
    /**
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config): self;
}
