<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Exceptions;

use Exception;

/**
 * Class NotImplementedException
 * @package Jtl\Vouchers\Api\Sdk\Exceptions
 */
class NotImplementedException extends Exception
{
}
