<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Exceptions;

use Exception;

/**
 * Class MissingApiClientException
 * @package Jtl\Vouchers\Api\Sdk\Exceptions
 */
class MissingApiClientException extends Exception
{
}
