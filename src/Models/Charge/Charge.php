<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Charge;

use DateTime;
use Exception;
use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Vouchers\Api\Sdk\Converter\AmountConverter;

class Charge extends Model
{
    public const TYPE_CHARGE = 'charge';
    public const TYPE_RECHARGE = 'recharge';

    /**
     * @var string|null
     */
    protected ?string $id;
    
    /**
     * @var string
     */
    protected string $voucherId;
    
    /**
     * @var string
     */
    protected string $clientId;
    
    /**
     * @var string - ^\d+\.\d{2}$
     */
    protected string $amount;
    
    /**
     * @var string
     */
    protected string $orderNumber;

    /**
     * @var string
     */
    protected string $type;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $createdAt = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $updatedAt = null;
    
    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    
    /**
     * @param string|null $id
     * @return self
     */
    public function setId(?string $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getVoucherId(): string
    {
        return $this->voucherId;
    }
    
    /**
     * @param string $voucherId
     * @return self
     */
    public function setVoucherId(string $voucherId): self
    {
        $this->voucherId = $voucherId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }
    
    /**
     * @param string $clientId
     * @return self
     */
    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }
    
    /**
     * @param string $amount
     * @return self
     */
    public function setAmount(string $amount): self
    {
        $this->amount = $amount;
        
        return $this;
    }

    /**
     * @return float
     */
    public function getAmountAsFloat(): float
    {
        return AmountConverter::toFloat($this->amount);
    }
    
    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }
    
    /**
     * @param string $orderNumber
     * @return self
     */
    public function setOrderNumber(string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Charge
     */
    public function setType(string $type): Charge
    {
        $this->type = $type;
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return self
     * @throws Exception
     */
    public function setCreatedAt($createdAt): self
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = new DateTime($createdAt);
        }
        
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
    
    /**
     * @param DateTime|string|null $updatedAt
     * @return self
     * @throws Exception
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        if ($updatedAt === null) {
            return $this;
        }
        
        if (is_string($updatedAt)) {
            $updatedAt = new DateTime($updatedAt);
        }
        
        $this->updatedAt = $updatedAt;
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', 'string'),
            new PropertyInfo('voucherId', 'string'),
            new PropertyInfo('clientId', 'string'),
            new PropertyInfo('amount'),
            new PropertyInfo('orderNumber'),
            new PropertyInfo('type'),
            new PropertyInfo('createdAt', DateTime::class, null),
            new PropertyInfo('updatedAt', DateTime::class, null)
        ]);
    }
}
