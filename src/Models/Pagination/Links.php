<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Pagination;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Links
 * @package Jtl\Vouchers\Api\Sdk\Models\Pagination
 */
class Links extends Model
{
    /**
     * @var string|null
     */
    protected ?string $first = null;
    
    /**
     * @var string|null
     */
    protected ?string $last = null;
    
    /**
     * @var string|null
     */
    protected ?string $prev = null;
    
    /**
     * @var string|null
     */
    protected ?string $next = null;
    
    /**
     * @return string|null
     */
    public function getFirst(): ?string
    {
        return $this->first;
    }
    
    /**
     * @param string|null $first
     * @return Links
     */
    public function setFirst(?string $first): Links
    {
        $this->first = $first;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getLast(): ?string
    {
        return $this->last;
    }
    
    /**
     * @param string|null $last
     * @return Links
     */
    public function setLast(?string $last): Links
    {
        $this->last = $last;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPrev(): ?string
    {
        return $this->prev;
    }
    
    /**
     * @param string|null $prev
     * @return Links
     */
    public function setPrev(?string $prev): Links
    {
        $this->prev = $prev;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNext(): ?string
    {
        return $this->next;
    }
    
    /**
     * @param string|null $next
     * @return Links
     */
    public function setNext(?string $next): Links
    {
        $this->next = $next;
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('first', 'string', null),
            new PropertyInfo('last', 'string', null),
            new PropertyInfo('prev', 'string', null),
            new PropertyInfo('next', 'string', null)
        ]);
    }
}
