<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Pagination;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Pagination
 * @package Jtl\Vouchers\Api\Sdk\Models\Pagination
 */
class Pagination extends Model
{
    /**
     * @var array
     */
    protected array $data = [];
    
    /**
     * @var Links
     */
    protected Links $links;
    
    /**
     * @var Meta
     */
    protected Meta $meta;
    
    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
    
    /**
     * @param array $data
     * @return Pagination
     */
    public function setData(array $data): Pagination
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return Links
     */
    public function getLinks(): Links
    {
        return $this->links;
    }
    
    /**
     * @param Links $links
     * @return Pagination
     */
    public function setLinks(Links $links): Pagination
    {
        $this->links = $links;
        
        return $this;
    }
    
    /**
     * @return Meta
     */
    public function getMeta(): Meta
    {
        return $this->meta;
    }
    
    /**
     * @param Meta $meta
     * @return Pagination
     */
    public function setMeta(Meta $meta): Pagination
    {
        $this->meta = $meta;
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('data', 'array', []),
            new PropertyInfo('links', Links::class, null, true),
            new PropertyInfo('meta', Meta::class, null, true)
        ]);
    }
}
