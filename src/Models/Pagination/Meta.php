<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Pagination;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Meta
 * @package Jtl\Vouchers\Api\Sdk\Models\Pagination
 */
class Meta extends Model
{
    /**
     * @var int
     */
    protected int $currentPage;
    
    /**
     * @var int
     */
    protected int $from;
    
    /**
     * @var int
     */
    protected int $lastPage;
    
    /**
     * @var int
     */
    protected int $perPage;
    
    /**
     * @var int
     */
    protected int $to;
    
    /**
     * @var int
     */
    protected int $total;
    
    /**
     * @var string|null
     */
    protected ?string $path = null;
    
    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }
    
    /**
     * @param int $currentPage
     * @return Meta
     */
    public function setCurrentPage(int $currentPage): Meta
    {
        $this->currentPage = $currentPage;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }
    
    /**
     * @param int $from
     * @return Meta
     */
    public function setFrom(int $from): Meta
    {
        $this->from = $from;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->lastPage;
    }
    
    /**
     * @param int $lastPage
     * @return Meta
     */
    public function setLastPage(int $lastPage): Meta
    {
        $this->lastPage = $lastPage;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }
    
    /**
     * @param int $perPage
     * @return Meta
     */
    public function setPerPage(int $perPage): Meta
    {
        $this->perPage = $perPage;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getTo(): int
    {
        return $this->to;
    }
    
    /**
     * @param int $to
     * @return Meta
     */
    public function setTo(int $to): Meta
    {
        $this->to = $to;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
    
    /**
     * @param int $total
     * @return Meta
     */
    public function setTotal(int $total): Meta
    {
        $this->total = $total;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
    
    /**
     * @param string|null $path
     * @return Meta
     */
    public function setPath(?string $path): Meta
    {
        $this->path = $path;
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('currentPage', 'int'),
            new PropertyInfo('from', 'int'),
            new PropertyInfo('lastPage', 'int'),
            new PropertyInfo('perPage', 'int'),
            new PropertyInfo('to', 'int'),
            new PropertyInfo('total', 'int'),
            new PropertyInfo('path', 'string', null)
        ]);
    }
}
