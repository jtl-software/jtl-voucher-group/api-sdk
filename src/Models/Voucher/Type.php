<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Voucher;

/**
 * Class Type
 * @package Jtl\Vouchers\Api\Sdk\Models\Voucher
 */
final class Type
{
    public const PRINT = 'print';
    public const DIGITAL = 'digital';
}
