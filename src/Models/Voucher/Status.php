<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Voucher;

/**
 * Class Status
 * @package Jtl\Vouchers\Api\Sdk\Models\Voucher
 */
final class Status
{
    public const INACTIVE = 'inactive';
    public const ACTIVE = 'active';
}
