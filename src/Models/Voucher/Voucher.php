<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Voucher;

use DateTime;
use Exception;
use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Vouchers\Api\Sdk\Converter\AmountConverter;
use Jtl\Vouchers\Api\Sdk\Converter\TaxRateConverter;

/**
 * Class Voucher
 * @package Jtl\Vouchers\Api\Sdk\Models\Voucher
 */
class Voucher extends Model
{
    /**
     * @var string|null
     */
    protected ?string $id = null;
    
    /**
     * @var string
     */
    protected string $clientId;
    
    /**
     * @var string
     */
    protected string $code;
    
    /**
     * @var string|null
     */
    protected ?string $pin = null;

    /**
     * @var string|null
     */
    protected ?string $sku = null;
    
    /**
     * @var string|null
     */
    protected ?string $batch = null;
    
    /**
     * @var string - ^\d+\.\d{2}$
     */
    protected string $amount;

    /**
     * @var string - ^\d+\.\d{2}$
     */
    protected string $remainingAmount;
    
    /**
     * @var string - ISO-4217
     */
    protected string $currency;
    
    /**
     * @var string
     */
    protected string $status = Status::INACTIVE;
    
    /**
     * @var bool
     */
    protected bool $taxable = false;
    
    /**
     * @var string|null
     */
    protected ?string $taxRate = null;
    
    /**
     * @var string
     */
    protected string $type = Type::DIGITAL;
    
    /**
     * @var string|null
     */
    protected ?string $data = null;
    
    /**
     * @var bool
     */
    protected bool $deletable = false;

    /**
     * @var int|null
     */
    protected ?int $validityValue = null;

    /**
     * @var string|null
     */
    protected ?string $validityInterval = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $validUntil = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $createdAt = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $updatedAt = null;
    
    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    
    /**
     * @param string|null $id
     * @return self
     */
    public function setId(?string $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }
    
    /**
     * @param string $clientId
     * @return self
     */
    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPin(): ?string
    {
        return $this->pin;
    }
    
    /**
     * @param string|null $pin
     * @return self
     */
    public function setPin(?string $pin): self
    {
        $this->pin = $pin;
        
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * @param string|null $sku
     * @return self
     */
    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }
    
    /**
     * @param string|null $batch
     * @return self
     */
    public function setBatch(?string $batch): self
    {
        $this->batch = $batch;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }
    
    /**
     * @param string $amount
     * @return self
     */
    public function setAmount(string $amount): self
    {
        $this->amount = $amount;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getRemainingAmount(): string
    {
        return $this->remainingAmount;
    }

    /**
     * @param string $remainingAmount
     * @return self
     */
    public function setRemainingAmount(string $remainingAmount): self
    {
        $this->remainingAmount = $remainingAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmountAsFloat(): float
    {
        return AmountConverter::toFloat($this->amount);
    }
    
    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }
    
    /**
     * @param string $currency
     * @return self
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }
    
    /**
     * @param string $status
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function getTaxable(): bool
    {
        return $this->isTaxable();
    }
    
    /**
     * @return bool
     */
    public function isTaxable(): bool
    {
        return $this->taxable;
    }
    
    /**
     * @param bool $taxable
     * @return self
     */
    public function setTaxable(bool $taxable): self
    {
        $this->taxable = $taxable;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTaxRate(): ?string
    {
        return $this->taxRate;
    }
    
    /**
     * @param string|null $taxRate
     * @return self
     */
    public function setTaxRate(?string $taxRate): self
    {
        $this->taxRate = $taxRate;
        
        return $this;
    }
    
    /**
     * @return float
     */
    public function getTaxRateAsFloat(): float
    {
        return TaxRateConverter::toFloat($this->taxRate);
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return $this->data;
    }
    
    /**
     * @param string|null $data
     * @return self
     */
    public function setData(?string $data): self
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }
    
    /**
     * @param bool $deletable
     * @return self
     */
    public function setDeletable(bool $deletable): self
    {
        $this->deletable = $deletable;
        
        return $this;
    }

    /**
     * @return int|null
     */
    public function getValidityValue(): ?int
    {
        return $this->validityValue;
    }

    /**
     * @param int|null $validityValue
     * @return Voucher
     */
    public function setValidityValue(?int $validityValue): Voucher
    {
        $this->validityValue = $validityValue;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValidityInterval(): ?string
    {
        return $this->validityInterval;
    }

    /**
     * @param string|null $validityInterval
     * @return Voucher
     */
    public function setValidityInterval(?string $validityInterval): Voucher
    {
        $this->validityInterval = $validityInterval;

        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getValidUntil(): ?DateTime
    {
        return $this->validUntil;
    }
    
    /**
     * @param DateTime|string|null $validUntil
     * @return self
     * @throws Exception
     */
    public function setValidUntil($validUntil): self
    {
        if ($validUntil === null) {
            return $this;
        }
    
        if (is_string($validUntil)) {
            $validUntil = new DateTime($validUntil);
        }
    
        $this->validUntil = $validUntil;
    
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return self
     * @throws Exception
     */
    public function setCreatedAt($createdAt): self
    {
        if ($createdAt === null) {
            return $this;
        }
    
        if (is_string($createdAt)) {
            $createdAt = new DateTime($createdAt);
        }
    
        $this->createdAt = $createdAt;
    
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
    
    /**
     * @param DateTime|string|null $updatedAt
     * @return self
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): self
    {
        if ($updatedAt === null) {
            return $this;
        }
    
        if (is_string($updatedAt)) {
            $updatedAt = new DateTime($updatedAt);
        }
    
        $this->updatedAt = $updatedAt;
    
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', 'string'),
            new PropertyInfo('clientId', 'string'),
            new PropertyInfo('code'),
            new PropertyInfo('pin', 'string', null),
            new PropertyInfo('sku', 'string', null),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('amount'),
            new PropertyInfo('remainingAmount'),
            new PropertyInfo('currency'),
            new PropertyInfo('status'),
            new PropertyInfo('taxable', 'bool', false),
            new PropertyInfo('taxRate', 'string', null),
            new PropertyInfo('type'),
            new PropertyInfo('data', 'string', null),
            new PropertyInfo('deletable', 'bool', false),
            new PropertyInfo('validityValue', 'int', null),
            new PropertyInfo('validityInterval', 'string', null),
            new PropertyInfo('validUntil', DateTime::class, null),
            new PropertyInfo('createdAt', DateTime::class, null),
            new PropertyInfo('updatedAt', DateTime::class, null)
        ]);
    }
}
