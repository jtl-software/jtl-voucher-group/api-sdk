<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models\Client;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use DateTime;
use Exception;

/**
 * Class Client
 * @package Jtl\Vouchers\Api\Sdk\Models\Client
 */
class Client extends Model
{
    /**
     * @var string|null
     */
    protected ?string $id;
    
    /**
     * @var string
     */
    protected string $clientGroupId;
    
    /**
     * @var string
     */
    protected string $name;
    
    /**
     * @var string
     */
    protected string $secret;
    
    /**
     * @var string[]
     */
    protected array $scopes = [];
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $createdAt = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $updatedAt = null;
    
    /**
     * @var DateTime|null
     */
    protected ?DateTime $connectedAt = null;
    
    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    
    /**
     * @param string|null $id
     * @return self
     */
    public function setId(?string $id): self
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getClientGroupId(): string
    {
        return $this->clientGroupId;
    }
    
    /**
     * @param string $clientGroupId
     * @return self
     */
    public function setClientGroupId(string $clientGroupId): self
    {
        $this->clientGroupId = $clientGroupId;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }
    
    /**
     * @param string $secret
     * @return self
     */
    public function setSecret(string $secret): self
    {
        $this->secret = $secret;
        
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }
    
    /**
     * @param string[] $scopes
     * @return self
     */
    public function setScopes(array $scopes): self
    {
        $this->scopes = $scopes;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return self
     * @throws Exception
     */
    public function setCreatedAt($createdAt): self
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = new DateTime($createdAt);
        }
        
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
    
    /**
     * @param DateTime|string|null $updatedAt
     * @return self
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): self
    {
        if ($updatedAt === null) {
            return $this;
        }
        
        if (is_string($updatedAt)) {
            $updatedAt = new DateTime($updatedAt);
        }
        
        $this->updatedAt = $updatedAt;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getConnectedAt(): ?DateTime
    {
        return $this->connectedAt;
    }
    
    /**
     * @param DateTime|string|null $connectedAt
     * @return self
     * @throws Exception
     */
    public function setConnectedAt($connectedAt): self
    {
        if ($connectedAt === null) {
            return $this;
        }
        
        if (is_string($connectedAt)) {
            $connectedAt = new DateTime($connectedAt);
        }
        
        $this->connectedAt = $connectedAt;
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', 'string'),
            new PropertyInfo('clientGroupId', 'string'),
            new PropertyInfo('name'),
            new PropertyInfo('secret'),
            new PropertyInfo('scopes', 'string', [], false, true),
            new PropertyInfo('createdAt', DateTime::class, null),
            new PropertyInfo('updatedAt', DateTime::class, null),
            new PropertyInfo('connectedAt', DateTime::class, null)
        ]);
    }
}
