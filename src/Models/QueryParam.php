<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class QueryParam
 * @package Jtl\Vouchers\Api\Sdk\Models
 */
class QueryParam extends Model
{
    /**
     * @var string
     */
    protected string $key = '';
    
    /**
     * @var string
     */
    protected string $value = '';
    
    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }
    
    /**
     * @param string $key
     * @return QueryParam
     */
    public function setKey(string $key): QueryParam
    {
        $this->key = $key;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
    
    /**
     * @param string $value
     * @return QueryParam
     */
    public function setValue(string $value): QueryParam
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('filter[%s]=%s', $this->getKey(), $this->getValue());
    }
    
    /**
     * @return string
     */
    public function build(): string
    {
        return (string) $this;
    }
    
    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return !empty($this->key) && !empty($this->value);
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('key'),
            new PropertyInfo('value')
        ]);
    }
}
