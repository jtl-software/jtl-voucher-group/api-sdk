<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Models;

use Izzle\Model\Model;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use InvalidArgumentException;

class Query extends Model
{
    // Directions
    public const DIR_ASC = 'asc';
    public const DIR_DESC = 'desc';
    
    /**
     * @var int
     */
    protected int $limit = 100;
    
    /**
     * @var int
     */
    protected int $offset = 0;
    
    /**
     * @var int
     */
    protected int $page = 1;
    
    /**
     * @var string
     */
    protected string $order = '';
    
    /**
     * @var string
     */
    protected string $dir = self::DIR_ASC;
    
    /**
     * @var QueryParam[]
     */
    protected array $params = [];
    
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
    
    /**
     * @param int $limit
     * @return Query
     */
    public function setLimit(int $limit): Query
    {
        $this->limit = $limit;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
    
    /**
     * @param int $offset
     * @return Query
     */
    public function setOffset(int $offset): Query
    {
        $this->offset = $offset;
        if ($this->limit > 0) {
            $this->page = $this->offset / $this->limit + 1;
        }
    
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }
    
    /**
     * @param int $page
     * @return Query
     */
    public function setPage(int $page): Query
    {
        $this->page = $page;
        if ($this->limit > 0) {
            $this->offset = ($this->page - 1) * $this->limit;
        }
    
        return $this;
    }
    
    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }
    
    /**
     * @param string $order
     * @return Query
     */
    public function setOrder(string $order): Query
    {
        $this->order = $order;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }
    
    /**
     * @param string $dir
     * @return Query
     */
    public function setDir(string $dir): Query
    {
        $this->dir = $dir;
        
        return $this;
    }
    
    /**
     * @return QueryParam[]
     */
    public function getParams(): array
    {
        return $this->params;
    }
    
    /**
     * @param QueryParam[] $params
     * @return Query
     */
    public function setParams(array $params): Query
    {
        foreach ($params as $param) {
            if (!($param instanceof QueryParam)) {
                throw new InvalidArgumentException(sprintf(
                    'Every entry must be instance of %s',
                    QueryParam::class
                ));
            }
        
            $this->params[$param->getKey()] = $param;
        }
    
        return $this;
    }
    
    /**
     * @param QueryParam $param
     * @return Query
     */
    public function addParam(QueryParam $param): Query
    {
        $this->params[$param->getKey()] = $param;
        
        return $this;
    }
    
    /**
     * @param string $key
     * @return QueryParam|null
     */
    public function getParam(string $key): ?QueryParam
    {
        if (!empty($this->params[$key])) {
            return $this->params[$key];
        }
        
        return null;
    }
    
    /**
     * @param QueryParam $param
     * @return Query
     */
    public function removeParam(QueryParam $param): Query
    {
        if (isset($this->params[$param->getKey()])) {
            unset($this->params[$param->getKey()]);
        }
        
        return $this;
    }
    
    /**
     * @param string $key
     * @return Query
     */
    public function removeParamByKey(string $key): Query
    {
        if (isset($this->params[$key])) {
            unset($this->params[$key]);
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('limit', 'int', 100),
            new PropertyInfo('offset', 'int', 0),
            new PropertyInfo('page', 'int', 1),
            new PropertyInfo('order'),
            new PropertyInfo('dir'),
            new PropertyInfo('params', QueryParam::class, [], true, true)
        ]);
    }
}
