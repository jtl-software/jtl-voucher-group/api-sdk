<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Izzle\Model\Model;
use JsonException;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Pagination;
use Jtl\Vouchers\Api\Sdk\Models\Query;
use Jtl\Vouchers\Api\Sdk\Models\Charge\Charge;
use Jtl\Vouchers\Api\Sdk\Models\Voucher\Voucher;

/**
 * Class VoucherResource
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
class VoucherResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws GuzzleException
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('vouchers', Voucher::class, $query);
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('vouchers/%s', $id), Voucher::class, $query);
    }

    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function findComplete(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('vouchers/%s/complete', $id), Voucher::class, $query);
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws GuzzleException
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Voucher::class);
    
        /** @var Voucher $model */
        return $this->create($model, 'vouchers', Voucher::class, $model->property('id'));
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws GuzzleException
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Voucher::class);
    
        /** @var Voucher $model */
        return $this->delete(sprintf('vouchers/%s', $model->getId()));
    }

    /**
     * @param string $amount
     * @param string $currency
     * @param string $orderNumber
     * @param string|null $id
     * @param string|null $code
     * @return bool
     * @throws GuzzleException
     * @throws JsonException
     */
    public function recharge(string $amount, string $currency, string $orderNumber, ?string $id = null, ?string $code = null): bool
    {
        if (empty($id) && empty($code)) {
            throw new InvalidArgumentException('Both parameters id and code cannot be empty');
        }

        $response = $this->getClient()->getHttp()->request(
            'POST',
            'vouchers/recharge',
            [
                'body' => json_encode([
                    'id' => $id,
                    'code' => $code,
                    'amount' => $amount,
                    'currency' => $currency,
                    'order_number' => $orderNumber
                ], JSON_THROW_ON_ERROR)
            ]
        );

        return $response->getStatusCode() === 201;
    }
    
    /**
     * @param string $voucherId
     * @param string $status
     * @param string $comment
     * @return bool
     * @throws GuzzleException
     * @throws InvalidArgumentException
     */
    public function setStatus(string $voucherId, string $status, string $comment = ''): bool
    {
        if (empty($voucherId) || empty($status)) {
            throw new InvalidArgumentException('The parameters id and status cannot be empty');
        }
        
        $response = $this->getClient()->getHttp()->request(
            'PATCH',
            sprintf('vouchers/%s/status', $voucherId),
            [
                'body' => json_encode([
                    'status' => $status,
                    'comment' => $comment
                ], JSON_THROW_ON_ERROR, 512)
            ]
        );
    
        return $response->getStatusCode() === 200;
    }
    
    /**
     * @param string $voucherId
     * @return bool
     * @throws GuzzleException
     */
    public function deleteReservation(string $voucherId): bool
    {
        $response = $this->getClient()->getHttp()->request(
            'DELETE',
            sprintf('vouchers/%s/reservation', $voucherId)
        );
    
        return $response->getStatusCode() === 204;
    }
    
    /**
     * @param string $voucherId
     * @param Query $query
     * @return Pagination
     * @throws GuzzleException
     * @throws JsonException
     */
    public function allCharges(string $voucherId, Query $query): Pagination
    {
        return $this->findAll(sprintf('vouchers/%s/charges', $voucherId), Charge::class, $query);
    }
    
    /**
     * @param string $voucherId
     * @param string $chargeId
     * @param Query|null $query
     * @return Charge|Model|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function findCharge(string $voucherId, string $chargeId, Query $query = null): ?Charge
    {
        return $this->findBy(sprintf('vouchers/%s/charges/%s', $voucherId, $chargeId), Charge::class, $query);
    }
}
