<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use GuzzleHttp\Exception\GuzzleException;

/**
 * Class ChargeResource
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
class ChargeResource extends Resource
{
    /**
     * @param string $chargeId
     * @return bool
     * @throws GuzzleException
     */
    public function cancel(string $chargeId): bool
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            sprintf('charges/%s/cancel', $chargeId)
        );
    
        return $response->getStatusCode() === 204;
    }
}
