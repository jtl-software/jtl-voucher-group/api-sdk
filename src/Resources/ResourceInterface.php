<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use Izzle\Model\Model;
use Jtl\Vouchers\Api\Sdk\Client;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Pagination;
use Jtl\Vouchers\Api\Sdk\Models\Query;

/**
 * Interface ResourceInterface
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
interface ResourceInterface
{
    /**
     * @return Client
     */
    public function getClient(): Client;
    
    /**
     * @param Query $query
     * @return Pagination
     */
    public function all(Query $query): Pagination;
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     */
    public function find(string $id, Query $query = null): ?Model;
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     */
    public function save(Model $model, Query $query = null): bool;
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     */
    public function remove(Model $model, Query $query = null): bool;
}
