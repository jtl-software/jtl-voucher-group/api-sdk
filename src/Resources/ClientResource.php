<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Jtl\Vouchers\Api\Sdk\Models\Client\Client;

/**
 * Class ClientResource
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
class ClientResource extends Resource
{
    /**
     * @param string $pin
     * @return Client|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function connect(string $pin): ?Client
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            'clients/connect',
            [
                'body' => json_encode([
                    'pin' => $pin
                ], JSON_THROW_ON_ERROR)
            ]
        );
    
        if ($response->getStatusCode() !== 200) {
            return null;
        }
        
        return new Client($this->extractData($response));
    }

    /**
     * @return bool
     * @throws GuzzleException
     */
    public function disconnect(): bool
    {
        $response = $this->getClient()->getHttp()->request('POST', 'clients/disconnect');

        return $response->getStatusCode() === 204;
    }
}
