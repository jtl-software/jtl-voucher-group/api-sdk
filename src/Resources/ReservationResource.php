<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Jtl\Vouchers\Api\Sdk\Models\Charge\Charge;
use Jtl\Vouchers\Api\Sdk\Models\Reservation\Reservation;

/**
 * Class ReservationResource
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
class ReservationResource extends Resource
{
    /**
     * @param string $amount - ^\d+\.\d{2}$
     * @param string $currency - ISO-4217
     * @param string $code
     * @param string|null $pin
     * @return Reservation|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function reserve(string $amount, string $currency, string $code, ?string $pin = null): ?Reservation
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            'reservations',
            [
                'body' => json_encode([
                    'amount' => $amount,
                    'currency' => $currency,
                    'code' => $code,
                    'pin' => $pin
                ], JSON_THROW_ON_ERROR)
            ]
        );
        
        if ($response->getStatusCode() !== 201) {
            return null;
        }
        
        return new Reservation($this->extractData($response));
    }
    
    /**
     * @param string $reservationId
     * @param string $orderNumber
     * @return Charge|null
     * @throws GuzzleException
     * @throws JsonException
     */
    public function charge(string $reservationId, string $orderNumber): ?Charge
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            sprintf('reservations/%s/charge', $reservationId),
            [
                'body' => json_encode([
                    'order_number' => $orderNumber
                ], JSON_THROW_ON_ERROR)
            ]
        );
    
        if ($response->getStatusCode() !== 201) {
            return null;
        }
    
        return new Charge($this->extractData($response));
    }
}
