<?php declare(strict_types=1);

namespace Jtl\Vouchers\Api\Sdk\Resources;

use DateTime;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;
use Izzle\Model\Model;
use Izzle\Model\PropertyInfo;
use Jtl\Vouchers\Api\Sdk\Exceptions\NotImplementedException;
use Jtl\Vouchers\Api\Sdk\Models\Query;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Links;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Meta;
use Jtl\Vouchers\Api\Sdk\Models\Pagination\Pagination;
use Jtl\Vouchers\Api\Sdk\Client;
use JsonException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Resource
 * @package Jtl\Vouchers\Api\Sdk\Resources
 */
abstract class Resource implements ResourceInterface
{
    /**
     * @var Client
     */
    protected Client $client;
    
    /**
     * Resource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        Model::$serializedDateTimeFormat = DateTime::RFC3339;
        
        $this->client = $client;
    }
    
    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws NotImplementedException
     */
    public function all(Query $query): Pagination
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     * @throws NotImplementedException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws NotImplementedException
     */
    public function save(Model $model, Query $query = null): bool
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws NotImplementedException
     */
    public function remove(Model $model, Query $query = null): bool
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }
    
    /**
     * @param array $data
     * @param string $class
     * @return Pagination
     */
    public function getPagination(array $data, string $class): Pagination
    {
        if (!isset($data['data'])) {
            throw new InvalidArgumentException('Response does not have an items property');
        }
        
        if (!isset($data['links'])) {
            throw new InvalidArgumentException('Response does not have an links property');
        }
        
        if (!isset($data['meta'])) {
            throw new InvalidArgumentException('Response does not have an meta property');
        }
        
        if (!is_array($data['data'])) {
            throw new InvalidArgumentException('Property data must be an array');
        }
        
        $items = [];
        foreach ($data['data'] as $item) {
            $items[] = new $class($item);
        }
        
        return new Pagination([
            'data' => $items,
            'links' => new Links($data['links']),
            'meta' => new Meta($data['meta']),
        ]);
    }
    
    /**
     * @param string $uri
     * @param string $class
     * @param Query $query
     * @return Pagination
     * @throws JsonException
     * @throws GuzzleException
     */
    protected function findAll(
        string $uri,
        string $class,
        Query $query
    ): Pagination {
        $response = $this->getClient()->getHttp()->request('GET', $uri, [
            'query' => $query->toArray(),
        ]);
        
        $data = $this->extractData($response);
        
        return $this->getPagination($data, $class);
    }
    
    /**
     * @param string $uri
     * @param string $class
     * @param Query|null $query
     * @return Model|null
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function findBy(string $uri, string $class, Query $query = null): ?Model
    {
        try {
            $response = $this->getClient()->getHttp()->request('GET', $uri, [
                'query' => $query ? $query->toArray() : []
            ]);
            
            return new $class($this->extractData($response));
        } catch (ClientException $e) {
            if ($e->getResponse() && $e->getResponse()->getStatusCode() !== 404) {
                throw $e;
            }
            
            return null;
        }
    }
    
    /**
     * @param Model $model
     * @param string $uri
     * @param string|null $class
     * @param PropertyInfo|null $pkProperty
     * @return bool
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function create(
        Model $model,
        string $uri,
        string $class = null,
        PropertyInfo $pkProperty = null
    ): bool {
        $response = $this->getClient()->getHttp()->request('POST', $uri, [
            'body' => json_encode($this->removeEmpty($model->toArray()), JSON_THROW_ON_ERROR, 512),
        ]);
        
        $id = null;
        if ($class !== null && $pkProperty !== null) {
            $setter = $pkProperty->setter();
            $getter = $pkProperty->getter();
            $entity = new $class($this->extractData($response));
            $id = $entity->{$getter}();
            $model->{$setter}($id);
        }
        
        return $response->getStatusCode() === 201;
    }
    
    /**
     * @param Model $model
     * @param string $uri
     * @return bool
     * @throws GuzzleException
     */
    protected function update(Model $model, string $uri): bool
    {
        $response = $this->getClient()->getHttp()->request('PATCH', $uri, [
            'body' => json_encode($this->removeEmpty($model->toArray()), JSON_THROW_ON_ERROR, 512),
        ]);
        
        return $response->getStatusCode() === 200;
    }
    
    /**
     * @param string $uri
     * @return bool
     * @throws GuzzleException
     */
    protected function delete(string $uri): bool
    {
        $response = $this->getClient()->getHttp()->request('DELETE', $uri);
        
        return $response->getStatusCode() === 204;
    }
    
    /**
     * @param ResponseInterface $response
     * @return array
     * @throws JsonException
     */
    protected function extractData(ResponseInterface $response): array
    {
        $json = $response->getBody()->getContents();

        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }
    
    /**
     * @param Model $model
     * @param string $fqn
     */
    protected function validModel(Model $model, string $fqn): void
    {
        if (!is_a($model, $fqn)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Expected parameter $model to be an instance of \'%s\', class \'%s\' given',
                    $fqn,
                    get_class($model)
                )
            );
        }
    }
    
    /**
     * Remove empty properties on data array
     *
     * @param array $haystack
     * @return array
     */
    protected function removeEmpty(array $haystack): array
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->removeEmpty($haystack[$key]);
            }
            
            if ($haystack[$key] === null || (is_array($haystack[$key]) && count($haystack[$key]) === 0)) {
                unset($haystack[$key]);
            }
        }
        
        return $haystack;
    }
}
