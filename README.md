# JTL Vouchers API SDK

> Simple Vouchers API SDK 
>
> https://vouchers.api.jtl-software.com/docs

## Installation

> Using npm:
 ```shell
 $ composer require jtl/vouchers-api-sdk
 ```

## Usage

```php
use Jtl\Vouchers\Api\Sdk\Client as VoucherClient;
use Jtl\Vouchers\Api\Sdk\Service as VoucherService;
use Jtl\Vouchers\Api\Sdk\Models\Voucher\Voucher;
use Jtl\Vouchers\Api\Sdk\Models\Query;

// Init Voucher SDK Client
$client = new VoucherClient([
    'base_uri' => 'https://api.somehost.test/v1/',  // RFC 3986
    'token' => 'some_jwt',  // Only if needed
]);

// Init Voucher SDK Service
$voucherService = VoucherService::boot($client);
$pagination = $voucherService->voucher()->all(
    (new Query())->setLimit(25)
);

/** @var Voucher $voucher */
foreach ($pagination->getData() as $voucher) {
    echo sprintf(
        '%s: %s %s',
        $voucher->getId(),
        $voucher->getAmount(),
        $voucher->getCurrency()
    );
}
```

## License

Copyright (c) 2020-present JTL-Software-GmbH

[MIT License](http://en.wikipedia.org/wiki/MIT_License)
