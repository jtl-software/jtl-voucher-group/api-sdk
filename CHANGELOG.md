CHANGELOG
=========

This changelog references the relevant changes (bug and security fixes)

* 0.12.4 (2022-02-11)

  * bug [Git] Fixed Git bug

* 0.12.3 (2022-02-11)

  * feature [Model] Added 'getVoucherTaxRateAsFloat' method to model Reservation

* 0.12.2 (2022-02-11)

  * feature [Converter] Added 'TaxRateConverter'
  * feature [Model] Added 'getTaxRateAsFloat' method to model Voucher

* 0.12.1 (2021-09-14)

  * feature [Model] Added 'type' to model Charge

* 0.12.0 (2021-09-14)
  
  * change [Global] Renamed 'usage' to 'charge'

* 0.11.0 (2021-08-19)

  * change [Resource] Removed Client Type from Client Resource 'connect' Method

* 0.10.0 (2021-07-29)
  
  * feature [Resource] Added new method 'recharge' to Voucher Resource
  * feature [Resource] Added new method 'findComplete' to Voucher Resource
  * feature [Resource] Added new method 'disconnect' to Client Resource
  * feature [Model] Added 'remainingAmount' to Voucher Model
  * feature [Model] Added 'validityValue' to Voucher Model
  * feature [Model] Added 'validityInterval' to Voucher Model
  * change [Resource] Added Client Type to Client Resource 'connect' Method

* 0.9.2 (2021-06-25)

  * feature [Model] Added new property 'sku' to vouchers model

* 0.9.1 (2021-06-23)

  * feature [Converter] Added Amount Converter
  * feature [Model] Added new method 'getAmountAsFloat' to Reservation, Usage and Voucher Model

* 0.9.0 (2021-02-10)

  * change [Resource] Changed Usage Cancel return value
  * fix [Resource] Fixed Reservation charge return value

* 0.8.0 (2021-02-05)

    * change [Model] Removed currency from reservation model
    * change [Model] Added taxable to reservation model
    * change [Model] Added tax_rate to reservation model

* 0.7.1 (2021-02-05)

    * fix [Model] Added default value to Reservation currency

* 0.7.0 (2021-02-05)

    * change [Lib] Push to new libs
    * change [Model] Added taxable and tax_rate to model
    * change [Model] Changed voucher type values
    * change [Resource] Added currency to reserve method

* 0.6.0 (2019-11-06)

    * change [Lib] Push to new model lib and guzzle 7.x support

* 0.5.0 (2019-10-12)

    * change [Resource] Removed Vouchers update and changed SetStatus Call
    * change [Model] Removed sku from Vouchers model

* 0.4.0 (2019-06-18)

    * change [Resource] Changed Client register method to connect
    * change [Model] Changed Register Model to Client
    * feature [Model] Added scopes, createdAt, updatedAt and connectedAt to client model

* 0.3.2 (2019-06-08)

    * feature [Resource] Added default value for reservation order number

* 0.3.1 (2019-06-08)

    * feature [Resource] Added reservation and usage Resource getter to service class

* 0.3.0 (2019-06-08)

    * feature [Resource] New Reservation and Usage Resource
    * change [Resource] Method setReservation in Voucher Resource was removed
    * change [Resource] Method addUsage in Voucher Resource was removed

* 0.2.0 (2019-05-26)

    * change [All] Changed namespace and name from voucher-cloud to vouchers

* 0.1.0 (2019-05-08)

    * change [All] Changed primary keys from int to string for all models
    * feature [Model] Added Voucher Reservation Model
    * change [Resource] Changed VoucherResource setReservation and removed the voucherId param
    * change [Resource] Changed VoucherResource setStatus params and added sku option

* 0.0.1 (2019-04-15)

    * feature [Model] Removed epid property from product identifier
    * feature [Model] Added publicUrl property to product picture
    * feature [Model] Changed purchasePrice property to netRetailPrice on product picture
    * feature [Resource] Added new findPictureByProductId method to global product resource
